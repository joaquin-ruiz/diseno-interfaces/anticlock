package com.example.anticlock;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

public class ExampleActivity extends AppCompatActivity {

    // Función heredada de AppCompatActivity donde inicializamos widgets al ser creados.
    // Bundle guardará el estado inicial de la creación de la Activity (para resets, otros...).
    // https://developer.android.com/reference/android/app/Activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Llamamos al onCreate padre (no sobreescrito) para ejecutar su código junto al nuestro.
        super.onCreate(savedInstanceState);
        // Establecemos el contenido de la activity desde un layout
        setContentView(R.layout.activity_example);
        // Recogemos el id de nuestra Toolbar y lo establecemos como ActionBar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        // Ejemplo de Listener para evento onClick().
        // https://developer.android.com/guide/topics/ui/ui-events?hl=es-419
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

   @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
