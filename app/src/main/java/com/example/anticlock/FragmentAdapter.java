package com.example.anticlock;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.anticlock.fragments.AlarmFragment;
import com.example.anticlock.fragments.ClockFragment;
import com.example.anticlock.fragments.TimerFragment;
import com.example.anticlock.fragments.WeatherFragment;

public class FragmentAdapter extends FragmentPagerAdapter {

    final private int NUM_TABS = 4;
    private Context mContext;


    public FragmentAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ClockFragment();
            case 1:
                return new TimerFragment();
            case 2:
                return new AlarmFragment();
            case 3:
                return new WeatherFragment();
            default: return new ClockFragment();
        }

    }

    @Override
    public int getCount() {
        return NUM_TABS;
    }

    // This determines the title for each tab
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return mContext.getString(R.string.clock);
            case 1:
                return mContext.getString(R.string.timer);
            case 2:
                return mContext.getString(R.string.alarm);
            case 3:
                return mContext.getString(R.string.weather);
            default:
                return mContext.getString(R.string.clock);
        }
    }
}
